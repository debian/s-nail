# Translation of s-nail debconf template to Swedish
# Copyright (C) 2024 Martin Bagge <brother@persilja.net>
# This file is distributed under the same license as the s-nail package.
#
# Martin Bagge <brother@persilja.net>, 2024
msgid ""
msgstr ""
"Project-Id-Version: s-nail\n"
"Report-Msgid-Bugs-To: s-nail@packages.debian.org\n"
"POT-Creation-Date: 2018-09-17 19:46+0200\n"
"PO-Revision-Date: 2024-08-23 15:44+0200\n"
"Last-Translator: Martin Bagge / <brother@persilja.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../s-nail.templates:1001
msgid "Should the dotlock helper be installed ‘setgid mail’?"
msgstr "Ska dotlock installeras med \"setgid mail\"?"

#. Type: boolean
#. Description
#: ../s-nail.templates:1001
msgid ""
"S-nail protects mbox files via fcntl(2) file-region locks during file "
"operations in order to avoid inconsistencies due to concurrent "
"modifications. By default system mbox files are also locked using "
"traditional dotlock files."
msgstr ""
"S-nail skyddar mbox-filer med fcntl(2)-baserade filregionlås vid "
"filoperationer för att undvika att filer hamnar ur synk på grund av "
"samtidiga ändringar i filerna. Som standard skyddas även systemets "
"mbox-filer med vanliga dotlock-fil-lås."

#. Type: boolean
#. Description
#: ../s-nail.templates:1001
msgid ""
"On Debian system users normally lack the permissions to create files in the "
"directory containing the system mailboxes (/var/mail/). In this case a "
"dedicated privileged (setgid mail) dotlock helper is needed, however this "
"may be a security concern."
msgstr ""
"På vanliga Debian-system har användare inte tillåtelse att skapa filer i "
"sökvägen där mailbox-filerna är placerade (/var/mail/). I dessa fall behövs "
"en separat hjälpprocess för dotlock med förhöjda rättigheter (setgid mail), "
"detta kan dock öppna för andra säkerhetsöverväganden."
